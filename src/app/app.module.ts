import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { ProfileComponent } from './profile/profile.component';
import { RepoListComponent } from './repo-list/repo-list.component';
import { RepoItemComponent } from './repo-item/repo-item.component';
import { UsersService } from './users.service';
import { ResultComponent } from './result/result.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { MainComponent } from './main/main.component';
import { PSearchBoxComponent } from './main/p-search-box/p-search-box.component';
import { PResultComponent } from './main/p-result/p-result.component';
import { PNotFoundComponent } from './main/p-not-found/p-not-found.component';
import { PProfileComponent } from './main/p-profile/p-profile.component';
import { PRepoListComponent } from './main/p-repo-list/p-repo-list.component';
import { PRepoItemComponent } from './main/p-repo-item/p-repo-item.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ProfileComponent,
    RepoListComponent,
    RepoItemComponent,
    ResultComponent,
    NotFoundComponent,
    SearchBoxComponent,
    MainComponent,
    PSearchBoxComponent,
    PResultComponent,
    PNotFoundComponent,
    PProfileComponent,
    PRepoListComponent,
    PRepoItemComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
