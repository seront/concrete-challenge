import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/users.service';
import { User } from 'src/app/user';

@Component({
  selector: 'app-p-profile',
  templateUrl: './p-profile.component.html',
  styleUrls: ['./p-profile.component.css']
})
export class PProfileComponent implements OnInit {
  user: User;
  constructor(private userService: UsersService) { }

  ngOnInit() {
    this.user = this.userService.user;
  }

}
