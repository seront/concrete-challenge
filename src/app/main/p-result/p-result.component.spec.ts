import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PResultComponent } from './p-result.component';

describe('PResultComponent', () => {
  let component: PResultComponent;
  let fixture: ComponentFixture<PResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
