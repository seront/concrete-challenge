import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/users.service';

@Component({
  selector: 'app-p-search-box',
  templateUrl: './p-search-box.component.html',
  styleUrls: ['./p-search-box.component.css']
})
export class PSearchBoxComponent implements OnInit {
  term: string;

  constructor(private userService: UsersService) { }

  ngOnInit() {
    this.term = this.userService.term;
    console.log('PSearchBoxComponent ngOnInit', this.term);
  }

}
