# ChallengeJS

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

Propuesta

Implementar una aplicación de cliente, que vea la API de GitHub y mostrar los repositorios de un usuario determinado. Esta aplicación debe funcionar en los navegadores más recientes del mercado.

 

API

https://developer.github.com/v3/

 

Endpoints Detalles de un usuario: https://api.github.com/users/{username}

 

Repositorios de un usuario:

https://api.github.com/users/{username}/repos

 

 

Layout compartido vía Zeplin.io 

https://zpl.io/VxYQp7g

 

 

usuario: desafío_concrete

contraseña: challengeaccepted

 

Navegación

Al buscar un usuario por el login de github, diríjase a la página de resultados de búsqueda. Si el usuario se encuentra para mostrar la página de detalles del usuario (el resultado resultante), de lo contrario muestra el mensaje descriptivo (Layout NotFound).

 

Requisitos

Yo, como usuario, deseo buscar por un usuario de GitHub;
Yo, como usuario, deseo ver los detalles de ese usuario que fue buscado (número de seguidores, número de seguidos, imagen del avatar, e-mail y bio);
Yo, como usuario, deseo ver la lista de los repositorios de ese usuario que fue buscado, ordenados por el número decreciente de estrellas;
 

Definición de listo

El diseño debe ser implementado de acuerdo con la especificación de Zeplin.io No es obligatorio el uso de un framework, pero recomendamos React.js o Angular. Es obligatorio el uso de rutas (Ex: /users/{username}/repos).

 

Criterios de evaluación

Organización del proyecto: La estructura del proyecto, documentación y uso de control de versiones;
Innovación tecnológica: El uso de tecnologías más recientes, siempre y cuando sean estables;
Coherencia: Si se cumplen los requisitos; 
Buenas prácticas: Si el proyecto sigue buenas prácticas de desarrollo, incluyendo seguridad, optimización, clean code, etc;
Control de Calidad: Si el proyecto posee calidad asegurada por pruebas automatizadas (por ejemplo, Jasmine, Mocha, Chai, Jest, etc).
 

Entrega

El desafío debe ser entregado por GitHub. La aplicación debe estar alojada (Heroku, Netlify, Firebase, Plunker, etc) Las URL deben enviarse por correo electrónico.

 

Plazo

El plazo de entrega 4 días.

 

¡Buena suerte!