export interface User {
    login: string;
    avatar_url: string;
    repos_url: string;
    name: string;
    company: string;
    bio: string;
    public_repos: number;
    followers: number;
    following: number;
}
