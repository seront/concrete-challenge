import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../user';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  usuario: User;
  constructor(private userService: UsersService) { }

  ngOnInit() {
    this.userService.usuario.subscribe(res => {
      this.usuario = res;
    }, error => {
      console.error('ProfileComponent ', error);
    });
  }

}
