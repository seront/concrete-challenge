import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private userService: UsersService, private router: Router) { }

  ngOnInit() {
  }

  search(term: string): void {
    // Push a search term into the observable stream.
    console.log('Buscando: ' + term);
    this.userService.searchPromise(term).subscribe(
      data => {
        console.log(data);
        this.userService.user = data;
        this.userService.term = term;
        this.router.navigate(['user', data.login, 'repos']);
      }, error => {
        console.error(error);
        this.router.navigate(['user', 'not-found']);
      }
    );
  }

}
