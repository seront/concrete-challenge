import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PRepoItemComponent } from './p-repo-item.component';

describe('PRepoItemComponent', () => {
  let component: PRepoItemComponent;
  let fixture: ComponentFixture<PRepoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PRepoItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PRepoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
