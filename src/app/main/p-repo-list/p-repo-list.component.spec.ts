import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PRepoListComponent } from './p-repo-list.component';

describe('PRepoListComponent', () => {
  let component: PRepoListComponent;
  let fixture: ComponentFixture<PRepoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PRepoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PRepoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
