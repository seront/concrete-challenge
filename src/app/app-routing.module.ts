import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { SearchComponent } from './search/search.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ResultComponent } from './result/result.component';
import { PResultComponent } from './main/p-result/p-result.component';
import { PNotFoundComponent } from './main/p-not-found/p-not-found.component';
import { PProfileComponent } from './main/p-profile/p-profile.component';

const routes: Routes = [
  { path: '', redirectTo: '/search', pathMatch: 'full' },
  { path: 'search', component: MainComponent },
  { path: 'user', component: PResultComponent, children: [
    {path: ':name/repos', component: PProfileComponent, pathMatch: 'full'},
    { path: 'not-found', component: PNotFoundComponent, pathMatch: 'full' },
  ] },
  { path: 'find', component: SearchComponent },
  { path: 'result', component: ResultComponent },
  { path: 'not-found', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
