import { Injectable } from '@angular/core';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  usuario: Observable<User>;
  user: User;
  term: string;
  userURL = 'https://api.github.com/users/';

  constructor(private http: HttpClient) { }

  search(term: string): Observable<User> {
    console.log('UsersService ' + term);
    return this.http
      .get<User>(this.userURL + term)
      .pipe(map(data => {
        console.log(data);
        return data;
      }), catchError(this.handleError));
  }

  searchPromise(term: string): Observable<User> {
    console.log('UsersService ' + term);
    return this.http
      .get<User>(this.userURL + term);
  }

  private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return observableThrowError(res.error || 'Server error');
  }

}
