import { Component, OnInit } from '@angular/core';
import { Subject, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  switchMap
} from 'rxjs/operators';
import { User } from '../user';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css'],
  providers: [UsersService]
})
export class SearchBoxComponent implements OnInit {

  private searchTerms = new Subject<string>();

  constructor(private userService: UsersService) { }

  search(term: string): void {
    // Push a search term into the observable stream.
    console.log('Buscando: ' + term);
    this.searchTerms.next(term);
  }

  ngOnInit() {
    this.userService.usuario = this.searchTerms.pipe(
      debounceTime(300), // wait for 300ms pause in events
      distinctUntilChanged(), // ignore if next search term is same as previous
      switchMap(
        (term) => {
          // switch to new observable each time
          // return the http search observable
          // or the observable of empty heroes if no search term
          console.log(term);
          return term ? this.userService.search(term) : of<User>({
            login: '',
            avatar_url: '',
            repos_url: '',
            name: '',
            company: '',
            bio: '',
            public_repos: null,
            followers: null,
            following: null
          });
        }
      ),
      catchError(error => {
        // TODO: real error handling
        console.log(`Error in component ... ${error}`);
        return of<User>({
          login: '',
          avatar_url: '',
          repos_url: '',
          name: '',
          company: '',
          bio: '',
          public_repos: null,
          followers: null,
          following: null
        });
      })
    );
  }

}
