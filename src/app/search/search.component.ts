import { Component, OnInit } from '@angular/core';
import { Subject, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  switchMap
} from 'rxjs/operators';
import { User } from '../user';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [UsersService]
})
export class SearchComponent implements OnInit {
  private searchTerms = new Subject<string>();

  constructor(private userService: UsersService) { }

  search(term: string): void {
    // Push a search term into the observable stream.
    console.log('Buscando: ' + term);
    this.searchTerms.next(term);
  }

  ngOnInit() {
    // this.userService.usuario = this.searchTerms.asObservable<User>();
     this.searchTerms.pipe(
      switchMap(
        (term) => {
          // switch to new observable each time
          // return the http search observable
          // or the observable of empty heroes if no search term
          console.log(term);
          return this.userService.usuario = term ? this.userService.search(term) : of<User>({
            login: '',
            avatar_url: '',
            repos_url: '',
            name: '',
            company: '',
            bio: '',
            public_repos: null,
            followers: null,
            following: null
          });
        }
      ),
      catchError(error => {
        // TODO: real error handling
        console.log(`Error in component ... ${error}`);
        return of<User>({
          login: '',
          avatar_url: '',
          repos_url: '',
          name: '',
          company: '',
          bio: '',
          public_repos: null,
          followers: null,
          following: null
        });
      })
    );

  }

}
